function userInfo() {

	let username = prompt("Enter your username:");
	if (username === "" || username === null) {
		alert("Input should not be empty!");
	}

	let password = prompt("Enter your password:");
	if (password === "" || password === null) {
		alert("Input should not be empty!");
	}

	let role = prompt("Enter your role:");
	if (role === "" || role === null) {
		alert("Input should not be empty!");
	} else if (role === "admin") {
		alert("Welcome back to the class portal, admin!");
	} else if (role === "teacher") {
		alert("Thank you for logging in, teacher!")
	} else if (role === "student") {
		alert("Welcome to the class portal, student!")
	} else {
		alert("Role out of range.")
	}

}

userInfo()

function checkAverage(grade1, grade2, grade3, grade4) {

	let average = Math.round(grade1, grade2, grade3, grade4);

	if (average <= 74) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
	} else if (average >= 75 && average <= 79) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
	} else if (average >= 80 && average <= 84) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
	} else if (average >= 85 && average <= 89) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
	} else if (average >= 90 && average <= 95) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
	} else if (average >= 96) {
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
	}

}

checkAverage();